# README #

Microsoft Visual C# 2015 project.

### What is this repository for? ###

v 0.1
Basically it's just a challenge to myself on creating evolution simulator that will demonstrate some kind of "life" developing on it's own.

### How do I get set up? ###
Just open the project with your Visual Studio 2015 or higher & you're ready to go.

### Who do I talk to? ###
If you want to contact me in some way you can use my [email](mailto:ser6722@yandex.ru)