﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvolutionSim
{
    public partial class MainWin : Form
    {
        //TEMORARY
        private Creature creature;
        private int cicle_time = 0;
        private bool expand = false;

        Bitmap bmp = new Bitmap(10,10);
        int[] cxy = null;
        int[] cxy1 = null;
        //TEMPORARY ENDS HERE
        int fps = 30; // We don't want to see more that 60 frames per second
        DateTime startTime, endTime;

        public MainWin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.creature = new Creature(7, 1000, 300, 300);
            startTime = DateTime.Now;
            bmp = new Bitmap(this.pictureBox1.Width, this.pictureBox1.Height);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.creature == null) return;
            foreach (cMuscle ms in this.creature.muscles)
            {
                ms.active = 300;
            }
            cicle_time = 0;
            expand = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.creature == null) return;
            cicle_time++;
            if (cicle_time > 50)
            {
                cicle_time = 0;
                expand = !expand;
            }
            //HERE ARE THE CALCULATIONS RESIDES
            foreach (cMuscle ms in this.creature.muscles)
            {
                if (ms.active != 0)
                {
                    int active_sign = (int)(ms.active / Math.Abs(ms.active));
                    ms.active = (Math.Abs(ms.active) - 1);
                    if (ms.active < 0) ms.active = 0;
                    ms.active = ms.active * active_sign;
                }
            }
            foreach(cNode nd in this.creature.nodes) {
                int fx = 0, fy = 0;//Force x & y
                double ftx;//Container for temporary force x
                int tstr;//Container for temporary strength
                foreach (cMuscle ms in this.creature.muscles)
                {
                    int expand_sign = 1; // sign to revert vecor
                    int expand_sign_m = 1; // sign to revert vecor if the muscle is overstretched
                    int quadra_error = 1; // if wee need to change vector direction due to loosing sign when counting forces;
                    if (!expand)//TODO: REMOVE HARDCODED EXPANSION. IT SHOULD BE SET BY ACTIVE VALUE
                        expand_sign = expand_sign * -1;
                    int lc = ms.lengthControl();
                    int[] np = nd.getPosition();
                    // if muscle overstretched we need to reverse it's force direction
                    if ((lc > 0) && (expand_sign == -1)) expand_sign_m = -1;
                    if ((lc < 0) && (expand_sign == 1)) expand_sign_m = -1;

                    if (((ms.node1 == nd) || (ms.node2 == nd)) && (ms.active != 0))
                    {
                        int[] mp, mop; // muscle position & origin position relative to node
                        if (ms.node1 == nd)
                            mop = ms.node2.getPosition();
                        else
                            mop = ms.node1.getPosition();
                        mp = nd.getPosition();
                        //Lets translate that vector to the origin
                        mp[0] -= mop[0];
                        mp[1] -= mop[1];
                        if (mp[0] < 0) quadra_error = -1; // if we in the wrong quadrant
                        else quadra_error = 1;
                        //And finaly lets find that force vector for this particular muscle
                        float c;
                        tstr = (ms.getStrength() - nd.getFriction()) / 5;
                        if (tstr < 0) tstr = 0;
                        if (mp[0] != 0)
                        {
                            if (tstr != 0)
                            {
                                c = (float) mp[1] / mp[0];
                                ftx = (Math.Sqrt((tstr * tstr) / ( (c * c) + 1)));
                                fx += (int)(ftx) * expand_sign * expand_sign_m * quadra_error;
                                fy += (int)(c * ftx) * expand_sign * expand_sign_m * quadra_error;
                            }
                        }
                        else
                        {
                            fy = 0;
                            fx = tstr;
                        }
                        if(tstr == 0)
                        {
                            fx = 0;
                            fy = 0;
                        }
                        mp = nd.getPosition();
                        if(((mp[0] + fx) == mop[0]) && ((mp[1] + fx) == mop[1]))
                        {
                            fx += 1;
                            fy += 1;
                        }
                    }
                }
                nd.movePosition(fx, fy);
            }
            //DRAWING PART DOWN THERE
            endTime = DateTime.Now;
            if (((TimeSpan)(endTime - startTime)).TotalMilliseconds < (1000 / this.fps)) return;
            startTime = DateTime.Now;
            cxy = null;
            cxy1 = null;
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.Clear(Color.White);
                if (this.creature != null)
                {
                    for (int i = 0; i < creature.getNodeCount(); i++)
                    {
                        cxy = creature.nodes[i].getPosition();
                        g.FillEllipse(new System.Drawing.SolidBrush(Color.FromArgb((int)((255 * creature.nodes[i].getFriction()) / 100),255,0,0)), cxy[0], cxy[1], 10, 10);
                    }
                    for (int i = 0; i < creature.getMuscleCount(); i++)
                    {
                        cxy = creature.muscles[i].node1.getPosition();
                        cxy1 = creature.muscles[i].node2.getPosition();
                        g.DrawLine(new Pen(Color.Blue), cxy[0] + 5, cxy[1] + 5, cxy1[0] + 5, cxy1[1] + 5);
                    }
                }
            }
            this.pictureBox1.Image = bmp;
        }
    }
    public static class RandomFactory
    {
        public static Random val = new Random();
    }
    public class cNode
    {
        private int friction_limit;
        private int friction;
        private int x;
        private int y;

        public cNode(int max_friction)
        {
            this.friction_limit = max_friction;
            this.friction = this.friction_limit;
            this.setPosition(RandomFactory.val.Next(0, 101) + 300, RandomFactory.val.Next(0, 101) + 300);
        }

        public void setPosition(int cx, int cy)
        {
            this.x = cx;
            this.y = cy;
        }

        public void movePosition(int dx, int dy)
        {
            this.x += dx;
            this.y += dy;
        }

        public int[] getPosition()
        {
            return new int[] { this.x, this.y };
        }

        public int getFriction()
        {
            return this.friction;
        }

        public int changeFriction(int delta_val) // returns energy spent on changing it
        {
            int old_friction = this.friction;
            this.friction += delta_val;
            if (this.friction < 0) this.friction = 0;
            if (this.friction > this.friction_limit) this.friction = this.friction_limit;
            return (int) (((Math.Abs(old_friction - this.friction) * 100) / this.friction_limit) * this.friction_limit * 0.01);
        }
    }

    public class cMuscle
    {
        private int length_limit_max;
        private int length_limit_min;
        private int length;
        public int active = 0;
        private int strength;
        public cNode node1;
        public cNode node2;
        private int min_length_prc;

        public cMuscle(int mstrength, int min_length_percent, cNode cnode1, cNode cnode2)
        {
            this.strength = mstrength;
            this.node1 = cnode1;
            this.node2 = cnode2;
            min_length_prc = min_length_percent;
            int[] pos1 = this.node1.getPosition();
            int[] pos2 = this.node2.getPosition();
            int lx = Math.Abs(pos1[1] - pos1[0]);
            int ly = Math.Abs(pos2[1] - pos2[0]);
            this.length = (int) Math.Sqrt(lx * lx + ly * ly);
            this.length_limit_min = this.length / 100 * min_length_percent;
            this.length_limit_max = this.length;
        }

        public void recalculateLength()
        {
            int[] pos1 = this.node1.getPosition();
            int[] pos2 = this.node2.getPosition();
            int lx = Math.Abs(pos1[1] - pos1[0]);
            int ly = Math.Abs(pos2[1] - pos2[0]);
            this.length = (int)Math.Sqrt(lx * lx + ly * ly);
            this.length_limit_min = this.length / 100 * this.min_length_prc;
            this.length_limit_max = this.length;
        }

        public int changeLength(int delta_length) //returns energy cost of action
        {
            int prev_l = this.length;
            this.length = this.length + delta_length;
            if (this.length > this.length_limit_max) this.length = this.length_limit_max;
            if (this.length < this.length_limit_min) this.length = this.length_limit_min;
            return (int) (((Math.Abs(prev_l - this.length) * 100) / this.length_limit_max) * this.strength * 0.01);
        }

        public cNode[] getNodes()
        {
            return new cNode[] { this.node1, this.node2 };
        }

        public int getStrength()
        {
            return this.strength;
        }

        public int lengthControl()
        {
            int[] nd1, nd2;
            nd1 = this.node1.getPosition();
            nd2 = this.node2.getPosition();
            nd1[0] = Math.Abs(nd1[0] - nd2[0]);
            nd1[1] = Math.Abs(nd1[1] - nd2[1]);
            this.length = (int)Math.Sqrt(nd1[0] * nd1[0] + nd1[1] * nd1[1]);
            //this.changeLength(lgt - this.length);
            if (this.length > this.length_limit_max) return -1;
            if (this.length < this.length_limit_min) return 1;
            return 0;
        }

        public bool compareNodes(cMuscle mus)
        {
            cNode[] musNodes = mus.getNodes();
            if ((this.node1 == musNodes[0]) && (this.node2 == musNodes[1])) return true;
            if ((this.node1 == musNodes[1]) && (this.node2 == musNodes[0])) return true;
            return false;
        }

        public bool compareNodes(cNode nod1, cNode nod2)
        {
            if ((this.node1 == nod1) && (this.node2 == nod2)) return true;
            if ((this.node1 == nod2) && (this.node2 == nod2)) return true;
            return false;
        }
    }

    public class Creature
    {
        //Vital params
        private int energy;
        private int delta_energy;
        private int health;
        private int delta_health;
        private int lifetime; // in seconds
        //Node & muscules system
        public cNode[] nodes;
        private int nodeCount;
        private int[] nodeControl; // Used to colect data on muscles using nodes. If some node is not attached to any muscle that info can be found here;
        public cMuscle[] muscles;
        private int muscleCount;
        public Random random_factory = new Random();

        public Creature(int node_count, int life_time, int cenergy, int chealth)
        {
            this.lifetime = life_time;
            this.energy = node_count * 100 + cenergy;
            this.health = this.energy + chealth;
            this.delta_energy = cenergy;
            this.delta_health = chealth;
            this.nodeCount = node_count;
            this.muscleCount = RandomFactory.val.Next(node_count - 1, (int)((node_count * (node_count - 1)) / 2) + 1);
            this.nodes = new cNode[node_count];
            this.nodeControl = new int[node_count];
            this.muscles = new cMuscle[this.muscleCount];
            for(int i = 0; i <= node_count - 1; i++)
                this.nodes[i] = new cNode(RandomFactory.val.Next(0, 101));
            int length_percent = 0;
            int[] node_nums;
            for (int i = 0; i <= muscleCount - 1; i++)
            {
                length_percent = RandomFactory.val.Next(0, 101);
                node_nums = this.get2Nodes(this.muscles, this.nodes, i, this.nodeCount);
                this.muscles[i] = new cMuscle(RandomFactory.val.Next(0, 101), length_percent, this.nodes[node_nums[0]], this.nodes[node_nums[1]]);
                this.nodeControl[node_nums[0]] = 1;
                this.nodeControl[node_nums[1]] = 1;
            }
            if (this.controlMuscleAttachment()) this.reattachMuscules();
        }

        private bool controlMuscleAttachment()
        {
            bool ret = false;
            for(int i = 0; i < this.nodeCount; i++)
            {
                if (this.nodeControl[i] == 0) ret = true;
            }
            return ret;
        }

        private void reattachMuscules()
        {
            for (int i = 0; i < this.muscleCount; i++)
            {
                this.muscles[i].node1 = null;
                this.muscles[i].node2 = null;
            }
            this.nodeControl = new int[this.nodeCount];

            int[] node_nums;
            for (int i = 0; i <= muscleCount - 1; i++)
            {
                node_nums = this.get2Nodes(this.muscles, this.nodes, i, this.nodeCount);
                this.nodeControl[node_nums[0]] = 1;
                this.nodeControl[node_nums[1]] = 1;
                this.muscles[i].node1 = this.nodes[node_nums[0]];
                this.muscles[i].node2 = this.nodes[node_nums[1]];
                this.muscles[i].recalculateLength();
            }
            if (this.controlMuscleAttachment()) this.reattachMuscules();
        }

        public int getNodeCount()
        {
            return this.nodeCount;
        }

        public int getMuscleCount()
        {
            return this.muscleCount;
        }

        public cNode[] getNodes()
        {
            return this.nodes;
        }

        public cMuscle[] getMuscles()
        {
            return this.muscles;
        }

        private int[] get2Nodes(cMuscle[] fmuscules, cNode[] fnodes, int i, int total)
        {
            int[] ret = null;
            bool trig = false;
            int n1 = RandomFactory.val.Next(0, total );
            int n2 = RandomFactory.val.Next(0, total );
            if (n1 == n2) ret = get2Nodes(muscles, fnodes, i, total);
            else
                if (i > 0)
                for (int j = 0; j <= i; j++)
                    if ((trig == false) && (fmuscules[j] != null)) trig = fmuscules[j].compareNodes(fnodes[n1], fnodes[n2]);
            if (trig == true) ret = get2Nodes(fmuscules, fnodes, i, total);
            else
                if(ret == null) ret = new int[] { n1, n2 };
            return ret;
        }
    }
}
